package ru.konovalov.tm.api;

import ru.konovalov.tm.model.AbstractEntity;

import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    int size();

    void add(E entity);

    E findById(String id);

    void remove(E entity);

    E removeById(String id);

}
