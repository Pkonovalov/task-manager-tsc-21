package ru.konovalov.tm.api.repository;

import ru.konovalov.tm.api.IOwnerRepository;
import ru.konovalov.tm.model.Project;

public interface IProjectRepository extends IOwnerRepository<Project> {

    Project findOneByIndex(String userId, Integer index);

    Project findOneByName(String userId, String name);

    Project removeOneByIndex(String userId, Integer index);

    Project removeOneByName(String userId, String name);

    boolean existsByName(String userId, String name);

    String getIdByName(String userId, String name);

}
