package ru.konovalov.tm.api.repository;

import ru.konovalov.tm.api.IOwnerRepository;
import ru.konovalov.tm.api.IRepository;
import ru.konovalov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository extends IOwnerRepository<Task> {

    List<Task> findAll(final String userId, final Comparator<Task> comparator);

    List<Task> findAll(String userId);

    List<Task> findALLTaskByProjectId(String userId, String projectId);

    List<Task> removeAllTaskByProjectId(final String userId, final String projectId);

    Task assignTaskByProjectId(String userId, String projectId, String taskId);

    Task unassignTaskByProjectId(String userId, String taskId);

    void add(String userId, Task task);

    void remove(String userId, Task task);

    void clear(String userId);

    Task findOneById(String userId, String id);

    Task removeOneById(String userId, String id);

    Task findOneByIndex(String userId, Integer index);

    Task removeOneByIndex(String userId, Integer index);

    Task findOneByName(String userId, String name);

    Task removeOneByName(String userId, String name);

    void removeAllByProjectId(String userId, String projectId);

    int size(String userId);

    boolean existsById(String userId, String projectId);

    boolean existsByName(String userId, String name);

    abstract String getIdByIndex(int index);
}
