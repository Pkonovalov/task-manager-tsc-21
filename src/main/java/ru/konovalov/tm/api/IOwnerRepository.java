package ru.konovalov.tm.api;

import ru.konovalov.tm.model.AbstractOwner;
import ru.konovalov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IOwnerRepository <E extends AbstractOwner> extends IRepository<E> {

    List<E> findAll(String userId, Comparator<E> comparator);

    List<E> findAll(String userId);

    E findById(String userId, String id);

    int size(String userId);

    boolean existsById(String userId, String id);

    void clear(String userId);

    Project removeById(String userId, String id);

}
