package ru.konovalov.tm.command.task;

import ru.konovalov.tm.exeption.entity.TaskNotFoundException;
import ru.konovalov.tm.model.Task;
import ru.konovalov.tm.model.User;
import ru.konovalov.tm.util.TerminalUtil;

import static ru.konovalov.tm.util.TerminalUtil.incorrectValue;

public final class TaskByProjectIdBindCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-bind";
    }

    @Override
    public String description() {
        return "Bind task by project id";
    }

    @Override
    public void execute() {
        User user = serviceLocator.getAuthService().getUser();
        System.out.println("[ENTER TASK ID]");
        final String taskId = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().findOneById(user.getId(), taskId);
        if (task == null) throw new TaskNotFoundException();
        System.out.println("ENTER PROJECT ID");
        final String projectId = TerminalUtil.nextLine();
        final Task taskUpdated = serviceLocator.getProjectTaskService().assignTaskByProjectId(user.getId(), taskId, projectId);
        if (taskUpdated == null) incorrectValue();
    }
}
