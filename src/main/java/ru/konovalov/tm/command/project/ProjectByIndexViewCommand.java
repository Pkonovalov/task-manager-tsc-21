package ru.konovalov.tm.command.project;

import ru.konovalov.tm.util.TerminalUtil;

public final class ProjectByIndexViewCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-view-by-index";
    }

    @Override
    public String description() {
        return "View project by index";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER INDEX:");
        serviceLocator.getProjectService().findOneByIndex(userId, TerminalUtil.nextNumber() - 1);
    }

}
