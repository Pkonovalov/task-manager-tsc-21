package ru.konovalov.tm.exeption.entity;

import ru.konovalov.tm.exeption.AbstractException;

public class EmailExistException extends AbstractException {

    public EmailExistException(String value) {
        super("Error. Email '" + value + "' already exist.");
    }

}
