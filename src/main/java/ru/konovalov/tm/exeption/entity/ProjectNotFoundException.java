package ru.konovalov.tm.exeption.entity;

import ru.konovalov.tm.exeption.AbstractException;

public class ProjectNotFoundException extends AbstractException {

    public ProjectNotFoundException() {
        super("Error. Project not found.");
    }

}
