package ru.konovalov.tm.exeption.empty;

import ru.konovalov.tm.exeption.AbstractException;

public class EmptyPasswordException extends AbstractException {

    public EmptyPasswordException() {
        super("Error. Password is empty...");
    }

}
