package ru.konovalov.tm.exeption.system;

import ru.konovalov.tm.exeption.AbstractException;

public class IndexIncorrectException extends AbstractException {

    public IndexIncorrectException() {
        super("Error. Index is incorrect.");
    }

    public IndexIncorrectException(final Throwable cause) {
        super(cause);
    }

    public IndexIncorrectException(final String value) {
        super("Error. This value `" + value + "` is not number.");
    }

}
